#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.error;

import java.util.List;

public record ValidationError(String field, List<String> errors) {
}
