#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class DomainPage<T> {
    private List<T> content;

    private Integer page;

    private Integer pageSize;

    private Long totalElements;

    private Integer totalPages;
}
