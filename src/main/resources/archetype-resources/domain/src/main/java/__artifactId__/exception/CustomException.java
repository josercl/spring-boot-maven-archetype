#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.exception;

public class CustomException extends RuntimeException {
  CustomException(final String message) {
    super(message);
  }
}
