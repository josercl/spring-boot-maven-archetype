#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.exception;

@ErrorResponseStatus(404)
public class RecordNotFoundException extends CustomException {
  RecordNotFoundException(final String message) {
    super(message);
  }
}
