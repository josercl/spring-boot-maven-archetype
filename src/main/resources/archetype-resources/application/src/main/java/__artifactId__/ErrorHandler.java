#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId};

import ${package}.domain.error.ValidationError;
import ${package}.domain.exception.CustomException;
import ${package}.domain.exception.ErrorResponseStatus;
import ${package}.rest.server.model.ErrorDTO;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<ErrorDTO> handleCustomErrors(Throwable ex) {
        ErrorResponseStatus[] classAnnotations = ex.getClass().getAnnotationsByType(ErrorResponseStatus.class);
        ErrorResponseStatus[] superAnnotations = ex.getClass().getSuperclass().getAnnotationsByType(ErrorResponseStatus.class);

        List<ErrorResponseStatus> annotations = Stream.concat(Arrays.stream(classAnnotations), Arrays.stream(superAnnotations))
                .toList();

        int statusCode = annotations.stream()
                .findFirst()
                .map(ErrorResponseStatus::value)
                .orElse(HttpStatus.INTERNAL_SERVER_ERROR.value());
        ProblemDetail problemDetail = ProblemDetail.forStatus(statusCode);
        problemDetail.setDetail(ex.getMessage());
        return ResponseEntity.status(statusCode).body(problemDetail);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatusCode status,
            WebRequest request
    ) {
        List<ValidationError> errorList = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .collect(Collectors.groupingBy(
                        FieldError::getField,
                        Collectors.mapping(DefaultMessageSourceResolvable::getDefaultMessage, Collectors.toList())
                ))
                .entrySet()
                .stream()
                .map(entry -> new ValidationError(entry.getKey(), entry.getValue()))
                .toList();
        ProblemDetail problemDetail = ProblemDetail.forStatus(HttpStatus.UNPROCESSABLE_ENTITY);
        problemDetail.setProperty("errors", errorList);
        return ResponseEntity.unprocessableEntity().body(problemDetail);
    }
}
