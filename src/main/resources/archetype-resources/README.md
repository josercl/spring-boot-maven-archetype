# Generate server code from YAML
```shell
mvn generate-resources
```

# Generate client code from YAML
```shell
mvn -P client-side generate-resources
```

# Generate CRUD stuff (Entities, mapper, services, controller)

```shell
mvn generator:generateCrud -Dentities=entityNameHere
```