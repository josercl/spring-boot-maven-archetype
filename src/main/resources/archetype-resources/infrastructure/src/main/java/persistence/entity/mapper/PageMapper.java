#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.persistence.entity.mapper;

import ${package}.domain.model.DomainPage;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;

public interface PageMapper<T> {
    @Mapping(
            target = "pageSize",
            source = "size"
    )
    @Mapping(
            target = "page",
            source = "number"
    )
    @Mapping(
            target = "content",
            source = "content",
            defaultExpression = "java(java.util.List.of())"
    )
    DomainPage<T> toDomainPage(Page<T> page);
}
