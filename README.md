# How to use

## Edit settings.xml

Add profile to your $M2_HOME/settings.xml file

```xml
<settings>
  <profiles>
    <profile>
      <id>josercl-archetypes</id>
        <repositories>
          <repository>
            <id>archetype</id>
            <name>Repo for josercl archetypes</name>
            <url>https://gitlab.com/api/v4/projects/43841626/packages/maven</url>
            <releases>
              <enabled>true</enabled>
              <checksumPolicy>fail</checksumPolicy>
            </releases>
            <snapshots>
              <enabled>true</enabled>
              <checksumPolicy>warn</checksumPolicy>
            </snapshots>
          </repository>
        </repositories>
      </profile>
  </profiles>

  <pluginGroups>
    <pluginGroup>com.gitlab.josercl</pluginGroup>
  </pluginGroups>
  
    <!-- rest of settings.xml file -->
</settings>
```
## Create Project

```bash
mvn archetype:generate \
    -P josercl-archetypes \
    -DarchetypeGroupId=com.gitlab.josercl \
    -DarchetypeArtifactId=spring-boot-maven-archetype \
    -DarchetypeVersion=1.0.8 \
    -DgroupId=<your groupId> \
    -DartifactId=<your artifactId> \
    -Dversion=<your version> \
    -DinteractiveMode=false
```